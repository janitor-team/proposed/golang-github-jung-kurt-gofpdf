Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gofpdf
Source: https://github.com/jung-kurt/gofpdf
Files-Excluded: font/CalligrapherRegular.pfb
 		font/*.ttf
 		font/*.z

Files: *
Copyright: 2015-2019 Kurt Jung,
                     Olivier Plathey (author of FPDF where gofpdf is derived from),
                     Bruno Michel (valuable assistance with the code),
                     David Hernández Sanz (drawing support adapted from FPDF),
                     Martin Hall-May (transparency support adapted from FPDF),
                     Andreas Würmser (gradients and clipping from FPDF),
                     Manuel Cornes (outline bookmarks is)
                     and others (see doc/document.md)
License: MIT

Files: label.go
Copyright: 1988 Paul Heckbert
Comment: https://github.com/erich666/GraphicsGems
License: pre_open_source
 This code repository predates the concept of Open Source, and predates most
 licenses along such lines. As such, the official license truly is:
 .
 EULA: The Graphics Gems code is copyright-protected. In other words, you
 cannot claim the text of the code as your own and resell it. Using the code
 is permitted in any program, product, or library, non-commercial or
 commercial. Giving credit is not required, though is a nice gesture. The
 code comes as-is, and if there are any flaws or problems with any Gems code,
 nobody involved with Gems - authors, editors, publishers, or webmasters -
 are to be held responsible. Basically, don't be a jerk, and remember that
 anything free comes with no guarantee.

Files: debian/*
Copyright: 2020 Andreas Tille <tille@debian.org>
License: MIT

Files: utf8fontfile.go
Copyright: 2019, Arteom Korotkiy (Gmail: arteomkorotkiy)
License: ISC

Files: spotcolor.go
 makefont/doc.go
 template.go
 template_impl.go
Copyright: Kurt Jung
License: ISC


License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

